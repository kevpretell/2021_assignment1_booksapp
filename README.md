# Processo e Sviluppo Software - Assignment 1

## Repository
- https://gitlab.com/kevpretell/2021_assignment1_booksapp

## Componenti del gruppo 

- Pretell Kevin (816725)
- Cerizza Raffaele (845512)

## Applicazione
L’applicazione implementata costituisce una semplice Web App che consente la gestione di una libreria. Il linguaggio di programmazione utilizzato per implementare l’applicazione è Java. L’applicazione espone un’interfaccia che permette all’utente di eseguire le operazioni CRUD (Create, Read, Update, Delete). Pertanto l’utente può creare, recuperare, aggiornare e cancellare libri dalla libreria. Ogni libro è caratterizzato da tre attributi: un identificativo, l’autore e il titolo. I dati relativi ai libri vengono gestiti attraverso un database MySQL.

## CI/CD Pipeline
È stata realizzata una pipeline CI/CD che prevede i seguenti 7 stage:
1. **Build**
2. **Verify**
3. **Unit Test**
4. **Integration Test**
5. **Package**
6. **Release**
7. **Deploy**

## Build
Nella fase di Build viene effettuato il download delle dipendenze e viene compilato il codice sorgente dell'applicazione. Per la fase di build si è scelto di utilizzare l'ultima versione di Maven. Le dipendenze utilizzate dall'applicazione sono specificate nel file pom.xml. In particolare, le dipendenze utilizzate consentono di:
- avviare la Web App tramite Spring Boot Starter Web;
- gestire la persistenza dei dati tramite Spring Boot Starter Data JPA;
- utilizzare un sistema di gestione di database basato su MySQL;
- eseguire automaticamente i test.

## Verify
Nella fase di Verify vengono eseguite due attività: (i) una verifica dell'aderenza del codice a regole di codifica standard tramite Checkstyle; e (ii) un'analisi statica del codice volta a rilevare l'eventuale presenza di bug tramite SpotBugs. Anche per la fase di verify si è scelto di utilizzare l'ultima versione di Maven. Questo stage della pipeline è stato implementato in modo tale che queste due attività di verifica vengano eseguite parallelamente. Infatti queste due attività sono indipendenti e non richiedono una sequenzialità nell’esecuzione. Si è scelto di utilizzare una specifica cache per lo svolgimento di queste due attività di verifica in modo da velocizzare eventualmente l'esecuzione complessiva dello stage.  

## Unit Test
Nella fase di Unit Test viene effettuato il test dei metodi della classe Book. In particolare, l’unit test implementato prevede il test della creazione di un libro e dei metodi setter e getter della classe Book. Anche in questo caso è stato sfruttato Maven.

## Integration Test
Nella fase di Integration Test viene effettuato il test dell’interazione con il database MySQL. In particolare, l’integration test implementato prevede il test: (i) della creazione di un nuovo libro nel database; (ii) del recupero di dati dal database; e (iii) della eliminazione di un libro dal database. Il database MySQL è ospitato tramite il servizio offerto da Remote MySQL. Per l’esecuzione di questo test è stato utilizzato il plugin Maven Failsafe. 

## Package 
Nella fase di Package viene creato un file compresso .jar contenente tutti i file e le dipendenze utilizzate dall'applicativo, con la finalità di facilitare la sua distribuzione. Per il packaging vengono sfruttati Maven e Spring Boot tramite la funzione di repackage. Rispetto alla mera funzione package di Maven, la funzione repackage di Spring Boot consente di includere nel file .jar anche le dipendenze. Questo facilita l’esecuzione dell’artefatto .jar prodotto. Questo artefatto verrà poi utilizzato nella fase successiva.

## Release
Nella fase di Release viene creata un immagine Docker dell’applicazione contenente l'artefatto prodotto nella fase di Package. Per la creazione di questa immagine Docker viene utilizzato il servizio Docker-in-Docker di GitLab. L’immagine creata viene poi caricata sul GitLab Container Registry.

## Deploy 
Nella fase di Deploy viene effettuato il deploy dell’applicazione sfruttando il servizio di cloud platforming Heroku. In particolare in questa fase: (i) viene recuperata l’immagine Docker dell’applicazione dal GitLab Container Registry; (ii) viene associato un tag a questa immagine; (iii) viene caricata l’immagine su Heroku; e (iv) viene sfruttata l’API di Heroku per rendere l’applicazione funzionante e disponibile. L’applicazione è disponibile al seguente link: https://heroku-gitlab-booksapp-staging.herokuapp.com/.   

## Problemi riscontrati 
I rallentamenti più importanti nello svolgimento della pipeline si sono verificati negli stage di Unit-test ed Integration-test e sono stati causati dalla difficoltà a utilizzare i servizi GitLab per la connessione al database MySQL. Inoltre sono state incontrate alcune difficoltà nello stage di Deploy a causa dell'utilizzo di porte dinamiche da parte di Heroku. 
