package it.unimib.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    /**
     * Repository of the books.
     */
    @Autowired
    private BookRepository repo;

    /**
     * Size of the books showed in a page.
     */
    public static final int SIZE = 5;

    /**
     * Method to list all books in a page.
     * @param pageNum number of the page
     * @param sortField attribute used to sort books
     * @param sortDir asc or desc sorting method
     * @return page of books
     */
    public Page<Book> listAll(final int pageNum,
            final String sortField, final String sortDir) {

        Pageable pageable = PageRequest.of(pageNum - 1, SIZE,
                sortDir.equals("asc")
                ? Sort.by(sortField).ascending()
                        : Sort.by(sortField).descending());

        return repo.findAll(pageable);
    }

    /**
     * Method to save a book in the repository.
     * @param book book that needs to be saved.
     */
    public void save(final Book book) {
        repo.save(book);
    }

    /**
     * Method to get a book from the repository.
     * @param id identifier of the book
     * @return Book object or null
     */
    public Book get(final Long id) {
        return repo.findById(id).get();
    }

    /**
     * Method to delete a book from the repository.
     * @param id identifier of the book
     */
    public void delete(final Long id) {
        repo.deleteById(id);
    }

}
