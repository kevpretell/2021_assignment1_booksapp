package it.unimib.library;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {

    /**
     * Service to create/edit/delete books.
     */
    @Autowired
    private BookService service;

    /**
     * Method for viewing the interface home page.
     * @param model the model of the page
     * @return page
     */
    @RequestMapping("/")
    public String viewHomePage(final Model model) {
        return viewPage(model, 1, "author", "asc");
    }

    /**
     * Method for viewing a page.
     * @param model the model of the page
     * @param pageNum number of the page
     * @param sortField field used to sort books
     * @param sortDir desc or asc sorting input
     * @return "index" string for the page
     */
    @RequestMapping("/page/{pageNum}")
    public String viewPage(final Model model,
            @PathVariable(name = "pageNum") final int pageNum,
            @Param("sortField") final String sortField,
            @Param("sortDir") final String sortDir) {

        Page<Book> page = service.listAll(pageNum, sortField, sortDir);

        List<Book> listBooks = page.getContent();

        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir",
                sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listBooks", listBooks);

        return "index";
    }

    /**
     * Method for showing a page to create books.
     * @param model the model of the page
     * @return "new_book" string for the page
     */
    @RequestMapping("/new")
    public String showNewBookForm(final Model model) {
        Book book = new Book();
        model.addAttribute("book", book);

        return "new_book";
    }

    /**
     * Method for saving a new book.
     * @param book that needs to be saved
     * @return "redirect:/" string
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveBook(@ModelAttribute("book") final Book book) {
        service.save(book);

        return "redirect:/";
    }

    /**
     * Method for editing a book.
     * @param id of the book to edit
     * @return ModelAndView object
     */
    @RequestMapping("/edit/{id}")
    public ModelAndView showEditBookForm(
            @PathVariable(name = "id") final Long id) {
        ModelAndView mav = new ModelAndView("edit_book");

        Book book = service.get(id);
        mav.addObject("book", book);

        return mav;
    }

    /**
     * Method for deleting a book.
     * @param id of the book to delete
     * @return "redirect:/" string
     */
    @RequestMapping("/delete/{id}")
    public String deleteBook(@PathVariable(name = "id") final Long id) {
        service.delete(id);

        return "redirect:/";
    }

}
