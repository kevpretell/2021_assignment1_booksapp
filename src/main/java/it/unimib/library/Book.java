package it.unimib.library;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Book {

    /**
     * Identifier of the book.
     */
    private Long id;

    /**
     * Author of the book.
     */
    private String author;

    /**
     * Title of the book.
     */
    private String title;

    protected Book() {
    }

    /**
     * Getter of the identifier of the book.
     * @return id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    /**
     * Getter of the author of the book.
     * @return author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Getter of the title of the book.
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter of the identifier of the book.
     * @param parameterId new identifier of the book
     */
    public void setId(final Long parameterId) {
        this.id = parameterId;
    }

    /**
     * Setter of the author of the book.
     * @param parameterAuthor new author of the book
     */
    public void setAuthor(final String parameterAuthor) {
        this.author = parameterAuthor;
    }

    /**
     * Setter of the title of the book.
     * @param parameterTitle new title of the book
     */
    public void setTitle(final String parameterTitle) {
        this.title = parameterTitle;
    }
}
