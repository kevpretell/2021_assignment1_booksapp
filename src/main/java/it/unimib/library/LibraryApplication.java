package it.unimib.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LibraryApplication extends SpringBootServletInitializer {

    /**
     * Main method to start the application.
     * @param args String arguments
     */
    public static void main(final String[] args) {
        SpringApplication.run(LibraryApplication.class, args);
    }

}
