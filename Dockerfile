FROM openjdk:8-jdk-slim
ENV TZ=Europe/Rome

RUN apt-get update && apt-get install -y curl

WORKDIR /srv/app/
COPY ./target/Library-1.0.jar ./booksapp.jar

EXPOSE 8080
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "booksapp.jar"]
